﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Gedcom.Net
{
    public class GedcomDocument
    {
        public string Version { get; private set; }

        public string Format { get; private set; }

        public string Language { get; private set; }

        public string Source { get; private set; }

        public Encoding Encoding { get; private set; }

        public List<Individual> Individuals { get; private set; }

        public List<Family> Families { get; private set; }

        public GedcomDocument(List<Individual> individuals, List<Family> families)
        {
            this.Individuals = individuals;
            this.Families = families;
        }

        public static GedcomDocument Load(string filePath)
        {
            var dateConverter = new DateConverter();

            var rawLines = File.ReadLines(filePath).ToArray();

            var lines = new SortedList<int, Line>();

            Line previousLine = null;
            for (int i = 0; i < rawLines.Length; i++)
            {
                var lineString = rawLines[i];
                var firstSpace = lineString.IndexOf(' ');

                if (firstSpace < 0)
                    continue;

                var levelStr = lineString.Substring(0, firstSpace);

                int level = 0;
                try
                {
                    level = Convert.ToInt32(levelStr);
                }
                catch (Exception)
                {
                    previousLine.AppendToData(lineString);
                }

                var secondSpace = lineString.Substring(firstSpace + 1).IndexOf(' ');

                var type = lineString.Substring(firstSpace + 1);

                if (secondSpace < 0)
                {
                    var line = new Line(i + 1, level, type);
                    lines.Add(i, line);
                    previousLine = line;
                    continue;
                }
                else
                {
                    type = lineString.Substring(firstSpace + 1, secondSpace);
                    secondSpace = secondSpace + firstSpace + 1;

                    string referenceId = null;
                    if (type.StartsWith("@") && type.EndsWith("@"))
                    {
                        referenceId = type.Replace("@", "");
                        type = lineString.Substring(secondSpace + 1);
                    }
                    else if (level > 0 && !string.IsNullOrWhiteSpace(previousLine.ReferenceId))
                    {
                        referenceId = previousLine.ReferenceId;
                    }

                    var data = lineString.Substring(secondSpace + 1);

                    var line = new Line(i + 1, level, type, referenceId, data);
                    lines.Add(i, line);
                    previousLine = line;
                    continue;
                }
            }

            var individuals = new List<Individual>();
            var families = new List<Family>();

            Individual individual = null;
            Family family = null;

            DataEntry parentDataEntry = null;
            Line lastLine = null;

            for (int i = 0; i < lines.Count; i++)
            {
                var currentLine = lines[i];

                if (currentLine.Level == 0)
                {
                    individual = null;
                    family = null;
                }

                if (currentLine.Type == DataType.Individual)
                {
                    individual = new Individual(currentLine.ReferenceId);
                    individuals.Add(individual);
                    continue;
                }
                else if (currentLine.Type == DataType.Family)
                {
                    family = new Family(currentLine.ReferenceId);
                    families.Add(family);
                    continue;
                }

                if (individual != null)
                {
                    var dataEntry = new DataEntry(currentLine.Type, currentLine.Data);

                    if (currentLine.Level == 1)
                    {
                        individual.AddDataEntry(dataEntry);
                        parentDataEntry = dataEntry;
                        continue;
                    }

                    if (parentDataEntry != null && lastLine != null)
                    {
                        if (lastLine.Level < currentLine.Level)
                        {
                            parentDataEntry.AddDataEntry(dataEntry);
                        }
                        else if (lastLine.Level > currentLine.Level)
                        {
                            parentDataEntry = new DataEntry(currentLine.Type, currentLine.Data);
                        }
                        else if (lastLine.Level == currentLine.Level)
                        {
                            parentDataEntry.AddDataEntry(dataEntry);
                        }
                    }
                }
                else if (family != null)
                {
                    var dataEntry = new DataEntry(currentLine.Type, currentLine.Data);

                    if (currentLine.Level == 1)
                    {
                        family.AddDataEntry(dataEntry);
                        parentDataEntry = dataEntry;
                        continue;
                    }

                    if (parentDataEntry != null && lastLine != null)
                    {
                        if (lastLine.Level < currentLine.Level)
                        {
                            parentDataEntry.AddDataEntry(dataEntry);
                        }
                        else if (lastLine.Level > currentLine.Level)
                        {
                            parentDataEntry = new DataEntry(currentLine.Type, currentLine.Data);
                        }
                        else if (lastLine.Level == currentLine.Level)
                        {
                            parentDataEntry.AddDataEntry(dataEntry);
                        }
                    }
                }

                lastLine = lines[i];
            }

            foreach (var item in families)
            {
                foreach (var data in item.DataEntries)
                {
                    if (data.Type == DataType.Husband)
                    {
                        data.Value = data.Value.ToString().Replace("@", "");
                        item.Husband = individuals.FirstOrDefault(i => i.Id.Equals(data.Value));
                    }
                    else if (data.Type == DataType.Wife)
                    {
                        data.Value = data.Value.ToString().Replace("@", "");
                        item.Wife = individuals.FirstOrDefault(i => i.Id.Equals(data.Value));
                    }
                    else if (data.Type == DataType.Child)
                    {
                        data.Value = data.Value.ToString().Replace("@", "");
                        item.AddChild(individuals.FirstOrDefault(i => i.Id.Equals(data.Value)));
                    }
                }
            }

            foreach (var item in individuals)
            {
                foreach (var entry in item.DataEntries)
                {
                    if (entry.Type == DataType.Name)
                    {
                        item.Name = entry.Value.ToString();
                    }
                    else if (entry.Type == DataType.Birth && entry.DataEntries.Length > 0)
                    {
                        var dateEntry = entry.DataEntries.FirstOrDefault(de => de.Type == DataType.Date);

                        if (dateEntry != null)
                        {
                            item.BirthDate = dateConverter.Convert(dateEntry.Value.ToString());
                        }
                    }
                    else if (entry.Type == DataType.Death && entry.DataEntries.Length > 0)
                    {
                        var dateEntry = entry.DataEntries.FirstOrDefault(de => de.Type == DataType.Date);

                        if (dateEntry != null)
                        {
                            item.DeathDate = dateConverter.Convert(dateEntry.Value.ToString());
                        }
                    }
                }
            }

            var gedcomDoc = new GedcomDocument(individuals, families)
            {
                Version = lines.Where(l => l.Value.Type == DataType.GedcomVersion).Select(l => l.Value.Data).FirstOrDefault(),
                Format = lines.Where(l => l.Value.Type == DataType.GedcomFileFormat).Select(l => l.Value.Data).FirstOrDefault(),
                Language = lines.Where(l => l.Value.Type == DataType.GedcomFileLanguage).Select(l => l.Value.Data).FirstOrDefault(),
                Source = lines.Where(l => l.Value.Type == DataType.GedcomFileSource).Select(l => l.Value.Data).FirstOrDefault(),
                Encoding = Encoding.GetEncoding(lines.Where(l => l.Value.Type == DataType.GedcomFileCharacterSet).Select(l => l.Value.Data).FirstOrDefault())
            };

            return gedcomDoc;
        }
    }
}