﻿using System.Collections.Generic;

namespace Gedcom.Net
{
    public class DataEntry : IDataEntry
    {
        private readonly List<IDataEntry> _dataEntries = new List<IDataEntry>();

        public DataType Type { get; }

        public object Value { get; set; }

        public IDataEntry[] DataEntries { get { return _dataEntries.ToArray(); } }
        
        public DataEntry(DataType type, object value, params IDataEntry[] dataEntries)
        {
            this.Type = type;
            this.Value = value;
            this._dataEntries.AddRange(dataEntries);
        }

        public void AddDataEntry(IDataEntry dataEntry)
        {
            if (dataEntry == null)
                return;

            this._dataEntries.Add(dataEntry);
        }
    }
}
