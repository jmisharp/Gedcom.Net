﻿using System.Collections.Generic;

namespace Gedcom.Net
{
    public class Family
    {
        private readonly List<DataEntry> _dataEntries = new List<DataEntry>();

        private readonly List<Individual> _children = new List<Individual>();

        public string Id { get; private set; }

        public Individual Husband { get; set; }

        public Individual Wife { get; set; }

        public Individual[] Children { get { return _children.ToArray(); } }

        public DataEntry[] DataEntries { get { return _dataEntries.ToArray(); } }

        public Family()
        {
        }

        public Family(string id, Individual husband = null, Individual wife = null, params Individual[] children)
        {
            this.Id = id;
            this.Husband = husband;
            this.Wife = wife;
            this._children.AddRange(children);
        }

        public void AddDataEntry(DataEntry dataEntry)
        {
            if (dataEntry == null)
                return;

            this._dataEntries.Add(dataEntry);
        }

        public void AddChild(Individual child)
        {
            if (child == null)
                return;

            this._children.Add(child);
        }
    }
}
