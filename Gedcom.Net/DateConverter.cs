﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Gedcom.Net.DataTypes;

namespace Gedcom.Net
{
    // http://wiki-en.genealogy.net/GEDCOM/DATE-Tag
    public class DateConverter : IDateConverter
    {
        private readonly string[] abbreviatedMonthNames = CultureInfo.InvariantCulture.DateTimeFormat.AbbreviatedMonthNames.Select(m => m.ToLower()).ToArray();

        private readonly Regex dateRegex = new Regex(@"(\d{1,2})?\s?([A-Z]{3})?\s?(\d{4})", RegexOptions.Compiled);

        private readonly Regex dateRangeRegex = new Regex(@"(\d{1,2})?\s?((?!BET|AND|BEF|AFT|\s)[A-Z]{3})?\s?(\d{4})", RegexOptions.Compiled);

        private readonly Regex datePeriodRegex = new Regex(@"(FROM|TO)?\s?(\d{1,2})?\s?([A-Z]{3})?\s?(\d{4})", RegexOptions.Compiled);

        public object Convert(string date)
        {
            if (string.IsNullOrWhiteSpace(date))
            {
                return new Date();
            }

            // Date range
            if (date.StartsWith("BET") && date.Contains("AND") || date.StartsWith("BEF") || date.StartsWith("AFT"))
            {
                return this.ConvertDateRange(date);
            }

            // Approximated date
            if (date.StartsWith("ABT") || date.StartsWith("CAL") || date.StartsWith("EST"))
            {
                return this.ConvertApproximatedDate(date);
            }

            // Date period
            if (date.StartsWith("FROM") || date.StartsWith("TO"))
            {
                return this.ConvertDatePeriod(date);
            }

            return this.ConvertDate(date);
        }

        private object ConvertDate(string date)
        {
            var match = this.dateRegex.Match(date.Trim());

            if (match == null || !match.Success)
            {
                return new Date();
            }

            int.TryParse(match.Groups[3].Value, out int year);
            var monthIndex = Array.IndexOf(this.abbreviatedMonthNames, match.Groups[2].Value.ToLower());
            int.TryParse(match.Groups[1].Value, out int day);

            var month = (monthIndex < 0 || monthIndex < 12) ? new int?(monthIndex + 1) : null;

            return new Date(year, month, day);
        }

        private object ConvertDateRange(string date)
        {
            var matches = this.dateRangeRegex.Matches(date.Trim());

            if (matches == null || matches.Count == 0)
            {
                return new DateRange();
            }

            if (matches.Count > 1)
            {
                var after = (Date)this.ConvertDate(matches[0].Value);
                var before = (Date)this.ConvertDate(matches[1].Value);
                return new DateRange(after, before);
            }
            else
            {
                var before = (Date)this.ConvertDate(matches[0].Value);
                return new DateRange(null, before);
            }
        }

        private object ConvertApproximatedDate(string date)
        {
            return new ApproximatedDate();
        }

        private object ConvertDatePeriod(string date)
        {
            var matches = this.datePeriodRegex.Matches(date.Trim());

            if (matches == null || matches.Count == 0)
            {
                return new DatePeriod();
            }

            if (matches.Count > 1)
            {
                var from = this.GetDatePeriodPart(matches[0]);
                var to = this.GetDatePeriodPart(matches[1]);
                return new DatePeriod(from, to);
            }
            else
            {
                if (matches[0].Groups[1].Value == "FROM")
                {
                    var from = this.GetDatePeriodPart(matches[0]);
                    return new DatePeriod(from, null);
                }

                if (matches[0].Groups[1].Value == "TO")
                {
                    var to = this.GetDatePeriodPart(matches[0]);
                    return new DatePeriod(null, to);
                }
            }

            return new DatePeriod();
        }

        private Date GetDatePeriodPart(Match match)
        {
            int.TryParse(match.Groups[4].Value, out int year);
            var monthIndex = Array.IndexOf(this.abbreviatedMonthNames, match.Groups[3].Value.ToLower());
            int.TryParse(match.Groups[2].Value, out int day);

            var month = (monthIndex < 0 || monthIndex < 12) ? new int?(monthIndex + 1) : null;

            return new Date(year, month, day);
        }
    }
}
