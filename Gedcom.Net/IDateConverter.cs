﻿namespace Gedcom.Net
{
    public interface IDateConverter
    {
        object Convert(string date);
    }
}