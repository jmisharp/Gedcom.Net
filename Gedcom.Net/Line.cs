﻿namespace Gedcom.Net
{
    internal class Line
    {
        public int LineNumber { get; private set; }

        public string ReferenceId { get; private set; }

        public int Level { get; private set; }

        public DataType Type { get; private set; }

        public string TypeText { get; private set; }

        public string Data { get; private set; }

        public Line(int lineNumber, int level, string type, string referenceId = null, string data = null)
        {
            this.LineNumber = lineNumber;
            this.Level = level;
            this.TypeText = type;
            this.Type = LineTypeConverter.GetLineType(type);
            this.ReferenceId = referenceId;
            this.Data = data;
        }

        public void AppendToData(string str)
        {
            this.Data += str;
        }
    }
}
