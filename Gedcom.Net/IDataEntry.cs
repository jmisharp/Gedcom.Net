﻿namespace Gedcom.Net
{
    public interface IDataEntry
    {
        DataType Type { get; }

        object Value { get; set; }

        IDataEntry[] DataEntries { get; }

        void AddDataEntry(IDataEntry dataEntry);
    }
}
