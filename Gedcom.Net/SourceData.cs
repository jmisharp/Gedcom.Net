﻿namespace Gedcom.Net
{
    using System;

    public class SourceData
    {
        public string Name { get; private set; }

        public DateTime PublicationDate { get; set; }

        public string Copyright { get; private set; }
    }
}
