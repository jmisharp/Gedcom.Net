﻿namespace Gedcom.Net
{
    // http://phpgedview.sourceforge.net/ged551-5.pdf
    public enum DataType
    {
        Header,
        GedcomFile,
        GedcomVersion,
        GedcomFileFormat,
        GedcomFileCharacterSet,
        GedcomFileLanguage,
        GedcomFileSource,
        Name,
        RtlSave,
        GedcomFileCorporation,
        GedcomFileDestination,
        Date,
        GedcomFileInfo,
        MyHeritageProjectId,
        MyHeritageExportedFromSiteId,
        Individual,
        UpdatedDate,
        GivenFirstName,
        GivenSurname,
        FormerName,
        Sex,
        Birth,
        Place,
        ChristianCelebration,
        Occupation,
        Event,
        EventType,
        Residence,
        Address,
        Street,
        City,
        Country,
        OwnFamily,
        ChildFamily,
        RecordId,
        UniqueId,
        MarriedName,
        PostalCode,
        Note,
        EmailAddress,
        Baptism,
        Death,
        DeathCause,
        Burial,
        Census,
        Confirmation,
        Pedigree,
        Immigration,
        Probate,
        Concatenation,
        ReferencePage,
        DataQuality,
        DataDescription,
        OriginalSourceDocumentText,
        IndividualEventRole,
        KnownChildrenCount,
        Family,
        Husband,
        Wife,
        Child,
        Marriage,
        Divorce,
        MarriageLicenseEvent,
        WritingTitle,
        Publication,
        MyHeritageType,
        MyHeritageId,
        Trailer,
        Unknown
    }
}