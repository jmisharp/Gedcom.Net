﻿using System;

namespace Gedcom.Net
{
    public class Source
    {
        public string Version { get; private set; }

        public string ProductName { get; private set; }

        public string Corporation { get; private set; }

        public string Address { get; private set; }

        public SourceData Data { get; private set; }

        public string Destination { get; private set; }

        public DateTime TransmissionDate { get; private set; }
    }
}
