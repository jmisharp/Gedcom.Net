﻿using System.Collections.Generic;

namespace Gedcom.Net
{
    public class Individual : IDataEntry
    {
        private readonly List<IDataEntry> _dataEntries = new List<IDataEntry>();

        public string Id { get; private set; }

        public string Name { get; set; }

        public object BirthDate { get; set; }

        public object DeathDate { get; set; }

        public IDataEntry[] DataEntries { get { return _dataEntries.ToArray(); } }

        public DataType Type { get; }

        public object Value { get; set; }

        public Individual(string id)
        {
            this.Id = id;
            this.Type = DataType.Individual;
        }
        
        public void AddDataEntry(IDataEntry dataEntry)
        {
            if (dataEntry == null)
                return;

            this._dataEntries.Add(dataEntry);
        }
    }
}