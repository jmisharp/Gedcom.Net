﻿namespace Gedcom.Net.DataTypes
{
    public struct ApproximatedDate
    {
        public Date Date { get; }

        public DateType DateType { get; }

        public ApproximatedDate(Date date, DateType dateType)
        {
            this.Date = date;
            this.DateType = dateType;
        }
    }
}
