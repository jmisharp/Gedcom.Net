﻿namespace Gedcom.Net.DataTypes
{
    public class DatePeriod
    {
        public Date? From { get; set; }

        public Date? To { get; set; }

        public DatePeriod(Date? from = null, Date? to = null)
        {
            this.From = from;
            this.To = to;
        }
    }
}
