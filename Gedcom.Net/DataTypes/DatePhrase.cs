﻿namespace Gedcom.Net.DataTypes
{
    public struct DatePhrase
    {
        public string Phrase { get; set; }
    }
}
