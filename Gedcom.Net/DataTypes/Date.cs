﻿using System;

namespace Gedcom.Net.DataTypes
{
    public struct Date
    {
        public bool Incomplete { get; }

        public int? Day { get; }

        public int? Month { get; }

        public int? Year { get; }

        public DateTime CompleteDate { get; set; }

        public Date(int? year = null, int? month = null, int? day = null)
        {
            this.Day = (day.GetValueOrDefault(0) > 0) ? day : null;
            this.Month = (month.GetValueOrDefault(0) > 0) ? month : null;
            this.Year = (year.GetValueOrDefault(0) > 0) ? year : null;
            this.Incomplete = !(this.Day.HasValue && this.Month.HasValue && this.Year.HasValue);

            if (this.Incomplete)
            {
                this.CompleteDate = DateTime.MinValue;
            }
            else
            {
                this.CompleteDate = new DateTime(year.Value, month.Value, this.Day.Value);
            }
        }
    }
}
