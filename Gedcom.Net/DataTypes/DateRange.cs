﻿namespace Gedcom.Net.DataTypes
{
    public struct DateRange
    {
        public Date? Before { get; }

        public Date? After { get; }

        public DateRange(Date after, Date before)
        {
            this.After = after;
            this.Before = before;
        }

        public DateRange(Date? after, Date? before)
        {
            this.After = after;
            this.Before = before;
        }
    }
}
