﻿namespace Gedcom.Net.DataTypes
{
    public enum DateType
    {
        About,
        Estimated,
        Calculated
    }
}
