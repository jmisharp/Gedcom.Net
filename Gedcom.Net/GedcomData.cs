﻿namespace Gedcom.Net
{
    public class GedcomData
    {
        public string Version { get; private set; }

        public string Form { get; private set; }
    }
}
